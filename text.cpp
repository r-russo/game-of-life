#include "text.h"

Text::Text(SDL_Renderer *renderer, TTF_Font *font) {
    this->renderer = renderer;
    this->font = font;

    texture.setRenderer(renderer);
    texture.setFont(font);
}

void Text::draw() {
    texture.render(x, y);
}

void Text::setColor(SDL_Color textColor) {
    this->textColor = textColor;
}

void Text::setPos(int x, int y) {
    this->x = x;
    this->y = y;
}

void Text::setText(std::string text) {
    this->text = text;
    texture.loadFromText(text, textColor);
}

std::string Text::getText() {
    return text;
}

int Text::getWidth() {
    return texture.getWidth();
}

int Text::getHeight() {
    return texture.getHeight();
}