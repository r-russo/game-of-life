#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "texture.h"

class Text {
    public:
        Text(SDL_Renderer *renderer, TTF_Font *font);
        void setText(std::string text);
        void setColor(SDL_Color textColor);
        void setPos(int x, int y);
        void draw();
        std::string getText();
        int getWidth();
        int getHeight();
    private:
        SDL_Renderer *renderer {};
        TTF_Font *font {};
        Texture texture {};
        std::string text {};
        SDL_Color textColor {};
        int x {};
        int y {};
};