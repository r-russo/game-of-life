#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "game_of_life.h"
#include "button.h"
#include "text.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int PX_SIZE = 8;

void game_loop(SDL_Renderer *renderer);

int main(int argc, char *argv[]) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize. SDL Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Window *window = SDL_CreateWindow("Game of Life", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

    if (window == nullptr) {
        printf("Window could not be created. SDL Error: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1,
         SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer == nullptr) {
        printf("Renderer could not be created. SDL Error: %s\n", SDL_GetError());
        return 1;
    }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        printf("SDL_image could not initialize. SDL_image Error: %s\n", IMG_GetError());
        return 1;
    }

    if (TTF_Init() == -1) {
        printf("SDL_ttf could not initialize. SDL_ttf Error: %s\n", TTF_GetError());
        return 1;
    }

    game_loop(renderer);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    renderer = nullptr;
    window = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
    return 0;
}

void game_loop(SDL_Renderer *renderer) {
    GameOfLife gol((SCREEN_HEIGHT - 100) / PX_SIZE,
                    (SCREEN_WIDTH - 20) / PX_SIZE, PX_SIZE, renderer);
    // gol.randomInit();
    // gol.start();

    int board_x {(SCREEN_WIDTH - gol.getWidth()) / 2};
    int board_y {PX_SIZE};

    int ui_x {50};
    int ui_y {board_y + gol.getHeight()};

    Button btnStart(renderer, "assets/button_play.png");
    Button btnPause(renderer, "assets/button_pause.png");
    Button btnStop(renderer, "assets/button_stop.png");
    Button btnNew(renderer, "assets/button_new.png");
    Button btnRandom(renderer, "assets/button_random.png");
    int verticalCenterButtons = ui_y + (SCREEN_HEIGHT - ui_y - btnStart.getHeight()) / 2;
    btnNew.setPos(ui_x, verticalCenterButtons);
    btnRandom.setPos(ui_x + 40, verticalCenterButtons);

    int horizontalCenterControls = (SCREEN_WIDTH - 2 * btnStart.getWidth()) / 2;
    btnStart.setPos(horizontalCenterControls - 20, verticalCenterButtons);
    btnPause.setPos(horizontalCenterControls - 20, verticalCenterButtons);
    btnStop.setPos(horizontalCenterControls + 20, verticalCenterButtons);

    TTF_Font *font = TTF_OpenFont("assets/font.ttf", 24);
    SDL_Color textColor {255, 255, 255, SDL_ALPHA_OPAQUE};
    Text iterationsText(renderer, font);
    iterationsText.setColor(textColor);

    bool quit = false;
    SDL_Event e;

    while (!quit) {
        while(SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            if (gol.isRunning()) {
                if (btnPause.handleEvents(&e)) {
                    gol.pause();
                }
            } else {
                if (btnStart.handleEvents(&e)) {
                    gol.start();
                }
            }

            if (btnStop.handleEvents(&e)) {
                gol.stop();
            };

            if (btnNew.handleEvents(&e)) {
                gol.clear();
            }

            if (btnRandom.handleEvents(&e)) {
                gol.randomInit();
            }
        }

        gol.update();
        iterationsText.setText("Iterations  " + std::to_string(gol.getIterations()));
        int verticalCenterText = ui_y + (SCREEN_HEIGHT - ui_y - iterationsText.getHeight()) / 2;
        iterationsText.setPos(SCREEN_WIDTH - 150 - iterationsText.getWidth() / 2, verticalCenterText);

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        gol.draw(board_x, board_y);

        if (gol.isRunning()) {
            btnPause.draw();
        } else {
            btnStart.draw();
        }
        btnStop.draw();
        btnNew.draw();
        btnRandom.draw();

        iterationsText.draw();

        SDL_RenderPresent(renderer);
    }
}