#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "texture.h"

class Button {
    public:
        Button(SDL_Renderer *renderer, std::string image_path, int x = 0, int y = 0);
        ~Button();
        bool handleEvents(SDL_Event *event);
        void setPos(int x, int y);
        bool getState();
        void draw();
        int getWidth();
        int getHeight();
    private:
        bool state {};
        SDL_Renderer *renderer {};
        Texture texture {};
        std::string text {};
        int x {};
        int y {};
        int width {};
        int height {};
};