#pragma once
#include <iostream>
#include <SDL2/SDL.h>

class Timer {
    public:
        Timer();

        void start();
        void stop();
        void pause();
        void unpause();

        uint32_t getTime();

        bool isStarted();
        bool isPaused();

    private:
        uint32_t mStartTicks;
        uint32_t mPausedTicks;

        bool mPaused;
        bool mStarted;
};