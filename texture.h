#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class Texture {
    public:
        Texture();
        Texture(SDL_Renderer *renderer, TTF_Font *font = nullptr);
        ~Texture();

        void setFont(TTF_Font *font);
        void setRenderer(SDL_Renderer *renderer);
        bool loadFromImage(std::string path);
        bool loadFromText(std::string text, SDL_Color textColor);
        void render(int x, int y, SDL_Rect *clip = nullptr);
        int getWidth();
        int getHeight();
    private:
        SDL_Renderer *renderer {};
        TTF_Font *font {};
        SDL_Texture *texture {};
        int width {};
        int height {};
};