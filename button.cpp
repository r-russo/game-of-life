#include "button.h"

Button::Button(SDL_Renderer *renderer, std::string image_path, int x, int y) {
    this->renderer = renderer;
    this->x = x;
    this->y = y;
    state = false;

    texture.setRenderer(renderer);
    texture.loadFromImage(image_path.c_str());

    width = texture.getWidth() / 2;
    height = texture.getHeight();
}

Button::~Button() {
}

bool Button::handleEvents(SDL_Event *event) {
    bool release = false;
    if (event->type == SDL_MOUSEBUTTONDOWN) {
        if (event->button.button == SDL_BUTTON_LEFT && state == false) {
            int mouse_x {};
            int mouse_y {};
            SDL_GetMouseState(&mouse_x, &mouse_y);
            if (mouse_x < x + 32 && mouse_x > x &&
                    mouse_y < y + 32 && mouse_y > y) {
                state = true;
            }
        }
    } else if (event->type == SDL_MOUSEBUTTONUP) {
        if (event->button.button == SDL_BUTTON_LEFT && state == true) {
            state = false;
            release = true;
        }
    }

    return release;
}

void Button::setPos(int x, int y) {
    this->x = x;
    this->y = y;
}

bool Button::getState() {
    return state;
}

void Button::draw() {
    SDL_Rect clipRect {};
    if (state) {
        clipRect = {width, 0, width, height};
    } else {
        clipRect = {0, 0, width, height};
    }

    texture.render(x, y, &clipRect);
}

int Button::getHeight() {
    return height;
}

int Button::getWidth() {
    return width;
}