#include "game_of_life.h"

GameOfLife::GameOfLife(int rows, int cols, int px_size, SDL_Renderer *renderer) {
    this->rows = rows;
    this->cols = cols;
    this->len = rows * cols;
    this->px_size = px_size;
    this->renderer = renderer;

    running = false;
    stopped = true;
    mouse_left_button_down = false;
    mouse_right_button_down = false;

    height = rows * px_size;
    width = cols * px_size;

    update_interval = 100;

    last_cells = new uint8_t[len];
    cur_cells = new uint8_t[len];
    for (int i = 0; i < len; i++) {
        last_cells[i] = 0;
        cur_cells[i] = 0;
    }
}

void GameOfLife::start() {
    running = true;
    stopped = false;
}

void GameOfLife::pause() {
    running = false;
}

void GameOfLife::stop() {
    running = false;
    stopped = true;
    iterations = 0;
}

void GameOfLife::randomInit() {
    clear();
    for (int i = 0; i < len; i++) {
        cur_cells[i] = rand() % 2;
    }
}

void GameOfLife::clear() {
    stop();
    for (int i = 0; i < len; i++) {
        cur_cells[i] = 0;
    }
}

void GameOfLife::handleEvents(SDL_Event *event) {

}

void GameOfLife::update() {
    if (running && SDL_GetTicks() - time_since_last_update > update_interval) {
        time_since_last_update = SDL_GetTicks();

        int changes = 0;  // if 0, then stop counting iterations

        // Copy array
        for (int i = 0; i < len; i++) {
            last_cells[i] = cur_cells[i];
        }

        int cnt_neighbors {};
        for (int i = 0; i < len; i++) {
            int current_row = i / cols;
            cnt_neighbors = 0;
            for (int c = -1; c <= 1; c++) {
                if (i + c < current_row * cols || i + c > (current_row + 1) * cols) {
                    continue;
                }
                for (int r = -1; r <= 1; r++) {
                    int cell = i + r*cols + c;
                    if (cell >= 0 && cell < len && (r != 0 || c != 0)) {
                        cnt_neighbors += last_cells[i + r*cols + c] == ALIVE;
                    }
                }
            }

            // Rules
            if ((last_cells[i] == ALIVE && cnt_neighbors < 2) ||
                    (last_cells[i] == ALIVE && cnt_neighbors >= 4)) {
                cur_cells[i] = DEAD;
                changes++;
            } else if (last_cells[i] == DEAD && cnt_neighbors == 3) {
                cur_cells[i] = ALIVE;
                changes++;
            }
        }
        if (changes > 0) {
            iterations++;
        } else {
            pause();
        }
    }
}

void GameOfLife::draw(int x, int y) {
    if (stopped) {
        int mouse_x {};
        int mouse_y {};
        uint32_t mouse_state = SDL_GetMouseState(&mouse_x, &mouse_y);
        int row = (mouse_y - y) / px_size;
        int col = (mouse_x - x) / px_size;

        if (row < rows || col < cols) {
            if (mouse_state & SDL_BUTTON(SDL_BUTTON_LEFT)) {
                cur_cells[col + row * cols] = ALIVE;
            } else if (mouse_state & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
                cur_cells[col + row * cols] = DEAD;
            }
        }
    }

    SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);
    for (int i = 0; i < len; i++) {
        if (cur_cells[i] == ALIVE) {
            int px = (i % cols) * px_size + x;
            int py = (i / cols) * px_size + y;
            SDL_Rect rect {px, py, px_size, px_size};
            SDL_RenderFillRect(renderer, &rect);
        }
    }

    SDL_SetRenderDrawColor(renderer, 60, 60, 60, SDL_ALPHA_OPAQUE);
    for (int r = 0; r <= rows; r++) {
        SDL_RenderDrawLine(renderer, x, y + r * px_size,
                                     x + width, y + r * px_size);
    }

    for (int c = 0; c <= cols; c++) {
        SDL_RenderDrawLine(renderer, x + c * px_size, y,
                                     x + c * px_size, y + height);
    }
}

uint8_t GameOfLife::getCellState(int row, int col) {
    return cur_cells[col + row * cols];
}

int GameOfLife::getWidth() {
    return width;
}

int GameOfLife::getHeight() {
    return height;
}

bool GameOfLife::isRunning() {
    return running;
}

int GameOfLife::getIterations() {
    return iterations;
}