#include "texture.h"

Texture::Texture() {
    texture = nullptr;
    font = nullptr;
}

Texture::Texture(SDL_Renderer *renderer, TTF_Font *font) {
    this->renderer = renderer;
    this->font = font;
    texture = nullptr;
}

Texture::~Texture() {
    if (texture != nullptr) {
        SDL_DestroyTexture(texture);
        texture = nullptr;
    }
}

void Texture::setFont(TTF_Font *font) {
    this->font = font;
}

void Texture::setRenderer(SDL_Renderer *renderer) {
    this->renderer = renderer;
}

bool Texture::loadFromImage(std::string path) {
    if (texture != nullptr) {
        SDL_DestroyTexture(texture);
        texture = nullptr;
    }

    SDL_Surface *surface = IMG_Load(path.c_str());
    if (surface == nullptr) {
        printf("Unable to render image surface. SDL_image Error: %s\n", IMG_GetError());
        return false;
    }

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    width = surface->w;
    height = surface->h;
    SDL_FreeSurface(surface);

    if (texture == nullptr) {
        printf("Unable to create texture. SDL Error: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

bool Texture::loadFromText(std::string text, SDL_Color textColor) {
    if (font == nullptr) {
        printf("Font not loaded\n");
        return false;
    }

    if (texture != nullptr) {
        SDL_DestroyTexture(texture);
        texture = nullptr;
    }

    SDL_Surface *surface = TTF_RenderText_Blended(font, text.c_str(), textColor);
    if (surface == nullptr) {
        printf("Unable to render text surface. SDL_ttf Error: %s\n", TTF_GetError());
        return false;
    }

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    width = surface->w;
    height = surface->h;
    SDL_FreeSurface(surface);

    if (texture == nullptr) {
        printf("Unable to create texture. SDL Error: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

void Texture::render(int x, int y, SDL_Rect *clip) {
    SDL_Rect renderRect {x, y, width, height};

    if (clip != nullptr) {
        renderRect.w = clip->w;
        renderRect.h = clip->h;
    }

    SDL_RenderCopy(renderer, texture, clip, &renderRect);
}

int Texture::getWidth() {
    return width;
}

int Texture::getHeight() {
    return height;
}