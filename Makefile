CC=g++
CFLAGS=-I -std=c++11 -Wall -Wextra

DEPS=game_of_life.h texture.h button.h text.h
_OBJ = main.o game_of_life.o texture.o button.o text.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

ODIR=obj
LIBS=-lSDL2 -lSDL2_image -lSDL2_ttf

$(ODIR)/%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

.PHONY: clean

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f $(ODIR)/*.o main

main.exe:
	
