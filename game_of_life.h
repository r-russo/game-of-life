#pragma once
#include <SDL2/SDL.h>
#include <iostream>
#include "texture.h"

enum CellStates {DEAD, ALIVE};

class GameOfLife {
    public:
        GameOfLife(int rows, int cols, int px_size, SDL_Renderer *renderer);
        void randomInit();
        void clear();
        void start();
        void pause();
        void stop();
        void handleEvents(SDL_Event *event);
        void update();
        void draw(int x, int y);
        int getWidth();
        int getHeight();
        bool isRunning();
        int getIterations();
    private:
        int rows {};
        int cols {};
        int len {};
        int width {};
        int height {};
        int px_size {};
        uint32_t time_since_last_update {};
        uint32_t update_interval {};
        int iterations {};

        bool running {};
        bool stopped {};
        bool mouse_left_button_down {};
        bool mouse_right_button_down {};

        uint8_t *last_cells {};
        uint8_t *cur_cells {};
        SDL_Renderer *renderer {};
        uint8_t getCellState(int row, int col);
};